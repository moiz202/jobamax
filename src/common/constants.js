let userObj = {
	UID:'uid',
	objectId:'objectId'
}

// SUBSCRIPTION TYPE HERE
let subType = {
	FREE:'Free',
	STANDARD_MONTH:'standard-month',
	PREMIUM_MONTH:'premium-month',
	EXPERIENCE_MONTH:'experience-month',
	STANDARD_YEAR:'standard-year',
	PREMIUM_YEAR:'premium-year',
	EXPERIENCE_YEAR:'experience-year'   
}

export {
    userObj,subType
};