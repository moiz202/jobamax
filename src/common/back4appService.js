import Parse  from 'parse'
// import axios from 'axios';
// jobamax account
Parse.serverURL = 'https://jobamax.b4a.io'; // This is your Server URL
Parse.initialize(
  's3gwYWzFlAYgtfSLTNWZVp6wpTQwj5ISs5XgF7lL', // This is your Application ID
  'a8LAo3flJEWV61zpb6PKeLJGpLbvmzLS3b1dW42C' // This is your Javascript key
);

// testing account 
// Parse.serverURL = "https://parseapi.back4app.com/";
// Parse.initialize(
//     "BrQCnwmJdL7vgS9NG453bhLK7F2TODhVEJ2mxuuL", 
//     "Bf6uynTiODrSmqNm8g3EJnI4uOOOBy99RBxO7Fnl"
//     ); 

//Parse app utills
const Users = Parse.Object.extend('Users')
const myUserObj = new Users();
//queries 
const userQuery = new Parse.Query(Users);
//declare  job and blog query object here
const Jobs = Parse.Object.extend('Jobs');
const Blogs = Parse.Object.extend('Blogs');

import notification from '@/common/notification.js';
import i18n from '@/i18n.js';
import router from '@/router/index' 

let back4appService = {};
 
//get user by object id
back4appService.getUserByObjectId = async (object) => {
    let response = await userQuery.get(object);
    return response;
}


//get user data according to some conditions
back4appService.isUserExist = async (key,value) => {
    let response = await userQuery.equalTo(key,value).find();
    // console.log(response)
    return response;
}



//save user data
back4appService.saveUser = async (payload) => {
	try{
		let result = await myUserObj.save(payload)
        return result;
	} catch(err){
		console.log(err)
	}
     
}
//update user profile data
back4appService.updateProfileInfo = async (objectId,payload) => {
	try{
	// let object = userQuery.get(objectId)
    userQuery.get(objectId).then((object) => {
		object.set('firstName', payload.firstName);
		object.set('phoneNumber', payload.phoneNumber);
        object.set('lastName', payload.lastName);
        object.set('email', payload.email);
        object.set('dialCode', payload.dialCode);
        object.set('isProfileSetup', true);
        object.set('day', payload.day);
        object.set('month', payload.month);
        object.set('year', payload.year);
        object.set('postalCode', payload.postalCode);
        object.set('gender', payload.gender);
        // let result = await object.save()
        object.save().then((response) => {
            console.log(response)
        })
    })

    return true
	} catch(err){
		console.log(err)
	}
}
//update account status
back4appService.updateAccountStatus = (objectId,status) => {
    return new Promise((resolve, reject) => {
        userQuery.get(objectId).then((object) => {
            object.set('status', status);
            object.save().then((response) => {
              return resolve(response);
            },
            (error) => {
               return reject(error);
            });
        })
    })
}


//delete user account
back4appService.deleteAccount = objectId => {
    return new Promise((resolve, reject) => {
        userQuery.get(objectId).then((object) => {
          object.destroy().then((response) => {
               return resolve(response);
          }, (error) => {
               return reject(error);
          });
        })
    })
}
//save user info
back4appService.saveUserInfo = async (payload) => {
    try{
        let result = await myUserObj.save(payload)
        return result;
    } catch(err){
        console.log(err)
    }
     
}
//
//update user profile data
back4appService.updateProfile = (objectId,payload) => {
    return new Promise((resolve, reject) => {
    // let object = userQuery.get(objectId)
      userQuery.get(objectId).then((object) => {
        object.set('firstName', payload.firstName);
        object.set('lastName', payload.lastName);
        object.set('day', payload.day);
        object.set('month', payload.month);
        object.set('year', payload.year);
        object.set('postalCode', payload.postalCode);
        object.set('gender', payload.gender);
        object.save().then((response) => {
           return resolve(response);
        },
        (error) => {
            return reject(error);
        })
      })
    })
}


//upload profile image
back4appService.uploadProfile = (objectId,image,fileObj) => {
    if(image && fileObj){
        const fileData = new Parse.File(fileObj.name, {base64:image}, fileObj.type);
        fileData.save()
            .then(saved => {
                userQuery.get(objectId).then((object) => {
                    object.set('Image', saved);
                    console.log("URL " + saved.url());
                    object.save()
                    .then(() =>{
                        notification.sendNotificaton({msg:i18n.t('message.profile_update'),type:'success'}) 
                        router.push(`/${i18n.locale}/my-profile`)
                    })
                })
            })
            .catch(error => {
                console.log(error)
            })
    }
}


//save your idea
back4appService.saveIdea = payload => {
    //object for idea
    const Ideas = Parse.Object.extend('Ideas')
    const myIdeaObj = new Ideas();
    return new Promise((resolve, reject) => {
            myIdeaObj.save(payload).then((response) => {
                payload.emailType = 1;
                // sendMailCommonFunction(payload)
                Parse.Cloud.run('sendEmailWithSendgrid', payload)
               return resolve(response);
            }, (error) => {
               return reject(error);
            });
        })
}

//function to save job query
back4appService.saveQueries = payload => {
    //object for query submission
    const Queries = Parse.Object.extend('Queries')
    const myQueryObj = new Queries();

    if(payload.resume && payload.fileObj){
        return new Promise((resolve, reject) => {
            const fileData = new Parse.File(payload.fileObj.name, {base64:payload.resume}, payload.fileObj.type);
            fileData.save()
                .then(saved => {
                    myQueryObj.set('name', payload.name);
                    myQueryObj.set('email', payload.email);
                    myQueryObj.set('resume', saved.url());
                    myQueryObj.save().then((response) => {
                        payload.emailType = 2;
                        // payload.attachment = saved.url();
                        payload.attachment = payload.resume;
                        payload.filename = payload.fileObj.name;
                        payload.filetype = payload.fileObj.type;
                        delete payload.resume;
                        delete payload.fileObj;
                        Parse.Cloud.run('sendEmailWithSendgrid', payload)
                        // sendMailCommonFunction(payload)
                       return resolve(response);
                    },         (error) => {
                       return reject(error);
                    })
            })
            .catch(error => {
                return reject(error);
            })
        })
    } else {
        return new Promise((resolve, reject) => {
            myQueryObj.set('name', payload.name);
            myQueryObj.set('email', payload.email);
            myQueryObj.set('resume', payload.resume);
            myQueryObj.save().then((response) => {
                payload.emailType = 2;
                // sendMailCommonFunction(payload)
                Parse.Cloud.run('sendEmailWithSendgrid', payload)
               return resolve(response);
            }, (error) => {
               return reject(error);
            });
        })
    }
}

//function to save job query
back4appService.saveJobApplied = payload => {
    //object for query submission
    const JobApply = Parse.Object.extend('JobApply')
    const myJobApplyObj = new JobApply();

    if(payload.resume && payload.fileObj){
        return new Promise((resolve, reject) => {
            const fileData = new Parse.File(payload.fileObj.name, {base64:payload.resume}, payload.fileObj.type);
            fileData.save()
                .then(saved => {
                    myJobApplyObj.set('jobId',payload.jobId);
                    myJobApplyObj.set('name', payload.name);
                    myJobApplyObj.set('email', payload.email);
                    myJobApplyObj.set('phoneNumber', payload.phoneNumber);
                    myJobApplyObj.set('additionalInformation', payload.additionalInformation);
                    myJobApplyObj.set('resume', saved.url());
                    myJobApplyObj.save().then((response) => {
                        payload.emailType = 3;
                        // payload.attachment = saved.url();
                        payload.attachment = payload.resume;
                        payload.filename = payload.fileObj.name;
                        payload.filetype = payload.fileObj.type;
                        delete payload.resume;
                        delete payload.fileObj;
                        // sendMailCommonFunction(payload)
                        Parse.Cloud.run('sendEmailWithSendgrid', payload)
                       return resolve(response);
                    },         (error) => {
                       return reject(error);
                    })
            })
            .catch(error => {
                return reject(error);
            })
        })
    } else {
        return new Promise((resolve, reject) => {
            myJobApplyObj.set('jobId',payload.jobId);
            myJobApplyObj.set('name', payload.name);
            myJobApplyObj.set('email', payload.email);
            myJobApplyObj.set('resume', payload.resume);
            myJobApplyObj.set('phoneNumber', payload.phoneNumber);
            myJobApplyObj.set('additionalInformation', payload.additionalInformation);
            myJobApplyObj.save().then((response) => {
                payload.emailType = 3;
                // sendMailCommonFunction(payload)
                Parse.Cloud.run('sendEmailWithSendgrid', payload)
               return resolve(response);
            }, (error) => {
               return reject(error);
            });
        })
    }
}


//function to send mail
/*function sendMailCommonFunction(data){
    // const postData = JSON.stringify(data);
    const options = {
        // url: 'https://api.applligent.com/',
        url:'http://localhost:5000/',
        method: 'PUT',
        headers: {
           'Content-Type': 'application/json'
        },
        data: data
    }
    axios(options)
    .then(res => console.log(res))
    .catch(error => console.log(error))
}*/

//function to get all jobs
back4appService.getJobs = async (pageNumber) => {
    let skip = (pageNumber - 1) * 5;
    var jobQuery = new Parse.Query(Jobs);
    const totalJobs = await jobQuery.count();
    jobQuery.descending("createdAt");
    jobQuery.limit(5); // limit to at most 10 results
    jobQuery.skip(skip); // skip the first 10 results
    return new Promise((resolve, reject) => {
        jobQuery.find().then((response) => {
            response.totalJobs = totalJobs;
            return resolve(response);
        }, (error) => {
            return reject(error);
        });
    })
}


//function to get single job
back4appService.getJob = (jobId) => {
    const jobQuery = new Parse.Query(Jobs);
    return new Promise((resolve, reject) => {
        jobQuery.get(jobId).then((response) => {
            return resolve(response);
        }, (error) => {
            return reject(error);
        });
    })
}

//function to get all blogs
back4appService.getBlogs = async (pageNumber) => {
    let skip = (pageNumber - 1) * 3;
    var blogQuery = new Parse.Query(Blogs);
    const totalBlogs = await blogQuery.count();
    blogQuery.descending("createdAt");
    blogQuery.limit(3); // limit to at most 10 results
    blogQuery.skip(skip); // skip the first 10 results
    return new Promise((resolve, reject) => {
        blogQuery.find().then((response) => {
            response.totalBlogs = totalBlogs;
            return resolve(response);
        }, (error) => {
            return reject(error);
        });
    })
} 

//function to get single blog
back4appService.getBlog = (blogId) => {
    const blogQuery = new Parse.Query(Blogs);
    return new Promise((resolve, reject) => {
        blogQuery.get(blogId).then((response) => {
            return resolve(response);
        }, (error) => {
            return reject(error);
        });
    })
}


//function to get latest one blog
back4appService.getLatestBlog = () => {
    var blogQuery = new Parse.Query(Blogs);
    blogQuery.descending("createdAt");
    // blogQuery.limit(1); 
    // blogQuery.skip(0); 
    return new Promise((resolve, reject) => {
        blogQuery.first().then((response) => {
            return resolve(response);
        }, (error) => {
            return reject(error);
        });
    })
}


/* IMPLEMENT USER LOGIN SYSTEM HERE **/
const user = new Parse.User()

back4appService.signUp = (username,password,link) => {
    console.log(link)
    return new Promise((resolve, reject) => {
        user.set('username', username);
        user.set('email', username);
        user.set('password', password);
        user.signUp().then((user) => {
            Parse.Cloud.run('sendgridEmail', { toEmail: username,link:link+user.id})
            if (typeof document !== 'undefined') resolve(user) 
        }).catch(error => {
            if (typeof document !== 'undefined') reject(error)
        });
    })
}

// check user exist or not by email
back4appService.isEmailExist = (email) => {
    const User = new Parse.User();
    const query = new Parse.Query(User);
    return new Promise((resolve, reject) => { 
        let response = query.equalTo('email',email).count();
        response
            .then(result => { 
                if(!result){
                    resolve(result) 
                } else {
                    reject({message:'Email address is already exist.'})
                }
            })
            .catch(error => { reject(error)});
    })
}


back4appService.recuiterSignUp = (username,password,link) => {
    const User = new Parse.User();
    const query = new Parse.Query(User);
    return new Promise((resolve, reject) => {
        let response = query.equalTo('email',username).equalTo('emailVerified',true).count();
        response.then(result => {
            if(!result){
                user.set('username', username);
                user.set('email', username);
                user.set('password', password);
                console.log(link)
                user.save().then((user) => {
                    console.log(user)
                    // Parse.Cloud.run('sendgridEmail', { toEmail: username,link:link+user.id})
                    if (typeof document !== 'undefined') resolve(user) 
                }).catch(error => {
                    console.log("error",)
                    if (typeof document !== 'undefined') reject(error)
                });
            } else {
                reject({message:'Account already exist for this email address.'})
            }
        })
        .catch(error => {
            if (typeof document !== 'undefined') reject(error)
        });
        
    })
}

back4appService.logIn = (username,password) => {
    return new Promise((resolve, reject) => {
        Parse.User.logIn(username,password).then((user) => {
            if (user.get('emailVerified')) {
                resolve(user) 
            } else {
                Parse.User.logOut();
                reject({message:i18n.t('message.verify_email')})
            }
        }).catch(error => {
            if (typeof document !== 'undefined') reject(error)
        });
    })
}

back4appService.logout = () => {
    return Parse.User.logOut()
}

//function to reset password
back4appService.resetPassword = (email) => {
    return new Promise((resolve, reject) => {
        Parse.User.requestPasswordReset(email).then((res) => {
            resolve(res) 
        }).catch(error => {
            reject(error)
        });
    })
}

//get current user login
back4appService.currentUser = () => {
    return Parse.User.current();
}

back4appService.checkUser = (objectId) => {
    const query = new Parse.Query(user);
    return new Promise((resolve, reject) => {
       query.get(objectId).then((user) => {
            if (typeof document !== 'undefined') resolve(user) 
        }).catch(error => {
            if (typeof document !== 'undefined') reject(error)
        });
    })
}

// job Seeker object
const JobSeeker = Parse.Object.extend('JobSeeker');
const myJobSeekerObj = new JobSeeker();

back4appService.saveJobSeeker = (payload) => {
    return new Promise((resolve, reject) => {
        myJobSeekerObj.save(payload).then((user) => {
            resolve(user) 
        }).catch(error => {
            reject(error)
        });
    })
}

back4appService.isSeekerExist = async (key,value) => {
    const query = new Parse.Query(JobSeeker);
    let response = await query.equalTo(key,value).find();
    return response;
}

/* RECUITER FUNCTONALITY HERE*/
const Recruiter = Parse.Object.extend('Recruiter');
const myRecruiterObj = new Recruiter();

back4appService.saveRecuiter = (payload) => {
    return new Promise((resolve, reject) => {
        myRecruiterObj.save(payload).then((user) => {
            resolve(user) 
        }).catch(error => {
            reject(error)
        });
    })
}

back4appService.saveRecuiterrrr = (payload) => {
    const user = new Parse.User()
    // return new Promise((resolve, reject) => {
        console.log(payload)
            user.set('username', payload.email);
            user.set('email', payload.email);
            user.set('password', payload.password);
            user.save().then((user) => {
                delete payload.password
                payload.userId = user.id
                console.log(user)
                console.log(payload)
                // myRecruiterObj.save(payload).then((user) => {
                //     resolve(user) 
                // }).catch(error => {
                //     reject(error)
                // });
            }).catch(error => {
                console.log(error)
            })
        // })
}

back4appService.isRecuiterExist = async (key,value) => {
    const query = new Parse.Query(Recruiter);
    let response = await query.equalTo(key,value).find();
    return response;
}

//update isSubscriptionDone = true in recuiter class
back4appService.updateRecuiter = (objectId,keyName) => {
    const query = new Parse.Query(Recruiter);
    return new Promise((resolve, reject) => {
        query.get(objectId).then((object) => {
            object.set(keyName, true);
            object.save().then((user) => {
                resolve(user) 
            },(error) => {
                reject(error)
            })
        },(error) => {
            reject(error)
        })
    })
}

//function to update plan time subscribe (standard and premium montly case)
back4appService.updatePlanRecuiter = (objectId,subscriptionPlan) => {
    //time 30 days from today
    const time = Math.round(new Date().getTime()/1000) + (30 * 24 * 60 * 60);
    const query = new Parse.Query(Recruiter);
    return new Promise((resolve, reject) => {
        query.get(objectId).then((object) => {
            object.set('subscriptionPlan', subscriptionPlan);
            object.set('paymentRecievedTime', time);
            object.save().then((user) => {
                resolve(user) 
            },(error) => {
                reject(error)
            })
        },(error) => {
            reject(error)
        })
    })
}

back4appService.currentSessionId = () => {
    Parse.Session.current()
    .then(function(session) {
    // Get the installation ID
    return session.get('installationId');
   });
}


//call custom cloud function
back4appService.editUserProperty = (objectId) => {
    return new Promise((resolve, reject) => {
        Parse.Cloud.run('editUserProperty', { objectId: objectId})
        .then(function(response) {
          resolve(response)
        }).catch(() => reject(null))
    })
}



/* END USER LOGIN SYSTEM **/
export default back4appService;

