import Vue from 'vue';
//sweet alert
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
const options = {
  confirmButtonColor: '#364fe4',
  cancelButtonColor: '#ff7674',
};
Vue.use(VueSweetalert2,options);

let notification = {};
// notification.sendNotificaton = (payload) => {
//     Vue.$toast.open({
//         message: payload.msg,
//         type:payload.type,
//         position:'bottom-right',
//         duration:5000,
//         pauseOnHover:true
//     });
// }

//setting for sweet alert
//error popup
notification.successPopup = (message) => {
    Vue.swal({
      icon: 'success',
      text: message,
      timer: 5000,
      confirmButtonText:'Okay'
    });
}
//success popup
notification.errorPopup = (message) => {
    Vue.swal({
      // icon: 'error',
      title: 'Oops...',
      text: message,
      confirmButtonText:'Okay',
      timer: 5000
    });
}
export default notification;