import Vue from 'vue';
import VueRouter from 'vue-router';

const Home = () => import( /* webpackChunkName: "home" */ '../views/Home.vue');
import i18n from '../i18n';
const SuccessComp = () => import( /* webpackChunkName: "SuccessComp" */ '../views/Success.vue');
const SuccessMobileComp = () => import( /* webpackChunkName: "SuccessMobileComp" */ '../views/MobileSuccess.vue')
const SuccessPayment  = () => import( /* webpackChunkName: "SuccessPayment" */ '../views/SuccessPayment.vue')
const SuccessPayment2  = () => import( /* webpackChunkName: "SuccessPayment2" */ '../views/SuccessPayment2.vue')
const SuccessAccount  = () => import( /* webpackChunkName: "SuccessAccount" */ '../views/SuccessAccount.vue')
const SuccessLogin = () => import( /* webpackChunkName: "SuccessLogin" */ '../views/SuccessLogin.vue')

Vue.use(VueRouter);

export default new VueRouter({
	mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: "active",
    linkExactActiveClass: "active",
    // base: '/',
    routes:[
        {
            path: '/',
            redirect: `/${i18n.locale}`
        },
        {
            path:'/:lang',
            component:{
                render (c) { return c('router-view')}
            },
            meta: { title: 'Jobs' },
            children:[
            {
				path:'/',
				name:'home',
				component: Home,
                meta: { title: 'Jobs' }
			},
			{
				path:'about-us',
				name:'about-us',
                meta: { title: 'About Us'},
				component: () => import( /* webpackChunkName: "about" */ '../views/AboutUs.vue')
			},
            {
                path:'job-seeker',
                name:'job-seeker',
                meta: { title: 'Job Seeker' },
                component: () => import( /* webpackChunkName: "JobSeeker" */ '../views/JobSeeker.vue')
            },
            {
                path:'professional',
                name:'professional',
                meta: { title: 'Recruiter' },
                component: () => import( /* webpackChunkName: "JobRecruiter" */ '../views/JobRecruiter.vue')
            },
            {
                path:'download',
                name:'download',
                meta: { title: 'Download' },
                component: () => import( /* webpackChunkName: "dawnload" */ '../views/Download.vue')
            },
            {
                path:'contact-us',
                name:'contact-us',
                meta: { title: 'Contact Us' },
                component: () => import( /* webpackChunkName: "ContactUs" */ '../views/ContactUs.vue')
            },
            {
                path:'security',
                name:'security',
                meta: { title: 'Security' },
                component: () => import( /* webpackChunkName: "Security" */ '../views/Security.vue')
            },
            {
                path:'job-list/:page?',
                name:'job-list',
                meta: { title: 'Job list' },
                component: () => import( /* webpackChunkName: "JobList" */ '../views/JobList.vue'),
            },
            {
                path:'details/:detailId',
                name:'job-details',
                meta: { title: 'Job Detail' },
                component: () => import( /* webpackChunkName: "JobDetails" */ '../views/JobDetails.vue')
            },
            {
                path:'community-guidelines',
                name:'community-guidelines',
                meta: { title: 'Community Guidelines' },
                component: () => import( /* webpackChunkName: "CommunityGuidelines" */ '../views/CommunityGuidelines.vue')
            },
            {
                path:'safety-tips',
                name:'safety-tips',
                meta: { title: 'Safety Tips' },
                component: () => import( /* webpackChunkName: "SafetyTips" */ '../views/SafetyTips.vue')
            },
            {
                path:'jobs/submit-application-form',
                name:'submit-application-form',
                meta: { title: 'Submit Application Form' },
                component: () => import( /* webpackChunkName: "SubmitApplicationForm" */ '../views/SubmitApplicationForm.vue')
            },
            {
                path:'privacy',
                name:'privacy',
                meta: { title: 'Privacy' },
                component: () => import( /* webpackChunkName: "Privacy" */ '../views/Privacy.vue')
            },
            {
                path:'terms',
                name:'terms',
                meta: { title: 'Terms of Use' },
                component: () => import( /* webpackChunkName: "Terms" */ '../views/Terms.vue')
            },
            {
                path:'subscription',
                name:'subscription',
                meta: { title: 'Subscription' },
                component: () => import( /* webpackChunkName: "Subscription" */ '../views/Subscription.vue')
            },
            {
                path:'submit-your-idea',
                name:'submit-your-idea',
                meta: { title: 'Submit Your Idea' },
                component: () => import( /* webpackChunkName: "SubmitYourIdea" */ '../views/SubmitYourIdea.vue')
            },
            {
                path:'support',
                name:'support',
                meta: { title: 'Support' },
                component: () => import( /* webpackChunkName: "Support" */ '../views/Support.vue')
            },
             {
                path:'profile',
                name:'profile',
                meta: { title: 'My Profile' },
                component: () => import(/* webpackChunkName: "Profile" */ '../views/Welcome.vue')
            },
            {
                path:'blogs/:page?',
                name:'blogs',
                meta: { title: 'Blog' },
                component: () => import(/* webpackChunkName: "BLog" */ '../views/Blog.vue')
            },
            {
                path:'blogs/details/:detailId',
                name:'blog-details',
                meta: { title: 'Detail' },
                component: () => import(/* webpackChunkName: "BlogDetails" */ '../views/BlogDetails.vue')
            },
            {
                path:'create-account',
                name:'create-account',
                meta: { title: 'create account' },
                component: () => import(/* webpackChunkName: "create-account" */'../views/CreateAccount.vue')
            },
            {
                path:'press',
                name:'press',
                meta: { title: 'Press' },
                component: () => import(/* webpackChunkName: "Press" */'../views/Press.vue')
            },
            // {
            //     path:'my-profile/cv',
            //     name:'cv',
            //     meta: { title: 'cv' },
            //     component: () => import('../views/Cv.vue')
            // },
            // {
            //     path:'my-profile',
            //     name:'my-profile',
            //     meta: { title: 'Profile',requiresAuth: true },
            //     component: () => import('../views/Profile.vue')
            // },
            // {
            //     path:'my-profile/account',
            //     name:'account',
            //     meta: { title: 'My Account',requiresAuth: true },
            //     component: () => import('../views/Account.vue')
            // },
            // {
            //     path:'my-profile/personal-info',
            //     name:'personal-info',
            //     meta: { title: 'personal info' },
            //     component: () => import('../views/PersonalInfo.vue')
            // },
            // {
            //     path:'chat',
            //     name:'chat',
            //     meta: { title: 'chat',requiresAuth: true },
            //     component: () => import('../views/Chat.vue')
            // },
            // {
            //     path:'training',
            //     name:'training',
            //     meta: { title: 'training',requiresAuth: true },
            //     component: () => import('../views/Training.vue')
            // },
            // {
            //     path:'jobs',
            //     name:'jobs',
            //     meta: { title: 'jobs',requiresAuth: true },
            //     component: () => import('../views/Jobs.vue')
            // },
            // {
            //     path:'my-profile/media',
            //     name:'media',
            //     meta: { title: 'media',requiresAuth: true },
            //     component: () => import('../views/Media.vue')
            // },
            // {
            //     path:'my-profile/recuiter-media',
            //     name:'recuiter-media',
            //     meta: { title: 'recuiter media' ,requiresAuth: true},
            //     component: () => import('../views/RecuiterMedia.vue')
            // },
            // {
            //     path:'recuiter-personal-info',
            //     name:'recuiter-personal-info',
            //     meta: { title: 'recuiter personal info' },
            //     component: () => import('../views/RecuiterPersonalInfo.vue')
            // },
            // {
            //     path:'my-profile/recuiter-news',
            //     name:'recuiter-news',
            //     meta: { title: 'recuiter news' },
            //     component: () => import('../views/RecuiterNews.vue')
            // },
            {
                path:'verify-email-success/:objectId',
                name:"verify-email-success",
                meta: { title: 'verify email success'},
                component: SuccessComp
            },
            {
                path:'success-payment',
                name:'success-payment',
                meta:{ title: 'success payment' },
                component: SuccessPayment
            },
            {
                path:'success-subsciption-payment',
                name:'success-subsciption-payment',
                meta:{ title: 'success payment' },
                component: SuccessPayment2
            },
            {
                path:'success-login',
                name:'success-login',
                meta:{ title: 'success login' },
                component: SuccessLogin
            },
            {
                path:'success-account',
                name:'success-account',
                meta:{ title: 'success account' },
                component: SuccessAccount
            },
            {
                path:'subscribe',
                name:'subscribe',
                meta: { title: 'subscribe' },
                component: () => import(/* webpackChunkName: "SubscriptionModal" */ '../views/SubscriptionModal.vue')
            },
            {
                path:'subscribes',
                name:'subscribes',
                meta: { title: 'subscribe' },
                component: () => import(/* webpackChunkName: "SubscriptionModal2" */ '../views/SubscriptionModal2.vue')
            },
            {
                path:'verify-email-link-success/:objectId',
                name:"verify-email-link-success",
                meta: { title: 'verify email success'},
                component: SuccessMobileComp
            },
            ]
        }
	],
    scrollBehavior(to) {
        if (to.hash) {
            return window.scrollTo({ top: document.querySelector(to.hash).offsetTop, behavior: 'smooth' });
        }
        return window.scrollTo({ top: 0, behavior: 'smooth' });
    }

})