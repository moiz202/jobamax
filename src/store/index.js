import Vue from 'vue';
import Vuex from 'vuex';
// import {auth} from '@/firebase.js'
import router from '@/router/index'
import i18n from '@/i18n.js';
import axios from 'axios'

import back4appService from '@/common/back4appService.js';
import notification from '@/common/notification.js';

Vue.use(Vuex);
export const store = new Vuex.Store({
	state:{
        userProfile:{},
        isLoggedInUser:false,
        isUserProfileSetup:false, 
        isSecondHeader:false,
        isHeaderClass:false,
        isGreyColorClass:false,
        codeList:[],
        isShowFooter:true,
        jobs:[],
        isLoading:false,
        blogs:[],
        totalBlogs:0,
        totalJobs:0
	},
	getters:{
    getSinglejob:state => (jobId) => {
      return state.jobs.find(job => job.jobId == jobId)
    },
    getSingleBlog:state => (blogId) => {
      return state.blogs.find(blog => blog.blogId == blogId)
    },

	},
	actions:{
		//function to save firebase login data to backfor app and check user exist or not 
		async saveUserData({commit},loginUser){
			var fullName=[],userData = {}
			if(loginUser.displayName){
                fullName = loginUser.displayName.split(' ');
			}
			if(fullName.length > 1){
                userData.firstName = fullName[0];
                userData.lastName = fullName[1];
			} else {
				userData.firstName = (loginUser.displayName)?loginUser.displayName:'';
				userData.lastName = '';
			}
			userData.uid = loginUser.uid;
			userData.email = (loginUser.email)?loginUser.email:'';
			userData.phoneNumber = loginUser.phoneNumber;
			userData.userType = (loginUser.userType)?loginUser.userType:0;
			userData.dialCode = loginUser.dialCode;
			userData.isProfileSetup = false;
			userData.status = 1;
			userData.createdBy = loginUser.loginCode;
			//save user data
			let response = await back4appService.saveUser(userData)
			//save profile data
			commit('setUserProfileData',response)
			commit('setUserLoginStatus', true)
		},

    //function to save user info 
    async saveUserInformation({commit},payload){
      let response = await back4appService.saveUserInfo(payload)
      if(response){
          localStorage.setItem('objectId',response.id)
          localStorage.setItem('isLoggedIn',true);
          commit('setUserProfileData',response);
          commit('setUserLoginStatus', true);
          localStorage.removeItem('userObj');
          var tags = [];
          let browserLang = navigator.language;
          let checkLang = browserLang.includes(['fr','fr-be','fr-ca','fr-lu','fr-ch'])
          if(payload.userType == 1) {
                if(i18n.locale == 'fr' || checkLang ){
                  tags = [{ name: "Register-Seeker-FR" , status: "active"},{ name: "French" , status: "active"}]
                } else {
                  tags = [{ name: "Register-Seeker-EN" , status: "active"},{ name: "English" , status: "active"}]
                }
            } 
            if(payload.userType == 2) {
                if(i18n.locale == 'fr' || checkLang ){
                   tags = [{ name: "Register-Recruiter-FR" , status: "active"},{ name: "French" , status: "active"}]
                } else {
                  tags = [{ name: "Register-Recruiter-EN" , status: "active"},{ name: "English" , status: "active"}]
                }
            } 
            payload.tags = tags;
            sendMailChimps(payload);
          return true;
      } else {
        return false;
      }
    },

    //function to save job seeker Infor
    async saveJobSeekerInfo({commit},payload){
      let response = await back4appService.saveJobSeeker(payload)
      if(response){
          localStorage.setItem('objectId',response.id)
          localStorage.setItem('isLoggedIn',true);
          commit('setSeekerAndRecuiterInfo',response);
          commit('setUserLoginStatus', true);
          localStorage.removeItem('userObj');
          var tags = [];
          let browserLang = navigator.language;
          let checkLang = browserLang.includes(['fr','fr-be','fr-ca','fr-lu','fr-ch'])
            if(i18n.locale == 'fr' || checkLang ){
                  tags = [{ name: "Register-Seeker-FR" , status: "active"},{ name: "French" , status: "active"}]
            } else {
                  tags = [{ name: "Register-Seeker-EN" , status: "active"},{ name: "English" , status: "active"}]
            }
            payload.tags = tags;
            sendMailChimps(payload);
          return true;
      } else {
        return false;
      }
    },
    //function to save job seeker Infor
    async saveRecuiterInfo({commit},payload){
      let response = await back4appService.saveRecuiter(payload)
      if(response){
          localStorage.setItem('objectId',response.id)
          localStorage.setItem('isLoggedIn',true);
          localStorage.removeItem('profileInfo')
          commit('setSeekerAndRecuiterInfo',response);
          commit('setUserLoginStatus', true);
          localStorage.removeItem('userObj');
          var tags = [];
          let browserLang = navigator.language;
          let checkLang = browserLang.includes(['fr','fr-be'],'fr-ca','fr-lu','fr-ch')
            if(i18n.locale == 'fr' || checkLang ){
              tags = [{ name: "Register-Recruiter-FR" , status: "active"},{ name: "French" , status: "active"}]
            } else {
              tags = [{ name: "Register-Recruiter-EN" , status: "active"},{ name: "English" , status: "active"}]
            }
            payload.tags = tags;
            sendMailChimps(payload);
          return true;
      } else {
        return false;
      }
    },

    //firebase methods here
    async fetchSeekerProfile({ commit },userId){
        if(userId){
          //fetch user profile
          let response = await back4appService.isSeekerExist("userId",userId);
          console.log(response)
          if(response.length){
            // set user login status login or not
            commit('setUserLoginStatus', true)
            localStorage.setItem('objectId', response[0].id)
            localStorage.setItem('isLoggedIn',true);
            localStorage.setItem('userType', 1);
            commit('setSeekerAndRecuiterInfo', response[0])
         } 
      }
    },

		//firebase methods here
		async fetchRecuiterProfile({ commit },user){
      if(user){
			//fetch user profile
			let response = await back4appService.isRecuiterExist("userId",user.uid);
      // console.log("recuiter response",response)
			if(response.length){
          // set user login status login or not
          commit('setUserLoginStatus', true)
          localStorage.setItem('objectId', response[0].id)
          localStorage.setItem('isLoggedIn',true);
          localStorage.setItem('userType', 2);
			commit('setSeekerAndRecuiterInfo', response[0])
			} 
			}
		},
		//update account information
		async updateProfile({commit},payload){
			let objectId = this.state.userProfile.objectId
			let isUpdate = await back4appService.updateProfileInfo(objectId,payload);
			if(isUpdate){
				this.state.isUserProfileSetup = true;
				let response = await back4appService.getUserByObjectId(objectId);
                commit('setUserProfileData', response)
                return true;
			}
		},
		//logout session
		async logout({ commit }) { 
        // log user out
        // await auth.signOut()
        await back4appService.logout()
        // clear user data from state
        commit('setUserProfile', {})
        //set login status false
        commit('setUserLoginStatus', false)
        localStorage.removeItem("uid");
        localStorage.removeItem("userId");  
        localStorage.removeItem('objectId')
        localStorage.removeItem('isLoggedIn')
        localStorage.removeItem('userObj')
        localStorage.removeItem('profileInfo')
        localStorage.clear()
        sessionStorage.clear()
        // redirect to login view
        router.push(`/${i18n.locale}/`)
       },

       //change header class
       changeHeader({ commit },payload){
         commit('setHeader', payload)
       },
       //change header list according to route
       changeHeaderClass({ commit },payload){
          commit('setHeaderClass', payload)
       },
       //change Header color 
       changeColor({ commit },payload){commit('setColorClass', payload)},
       //fetch country code from json file
       fetchCountryCode({ commit }){
       const countryData = require('./country-code.json')
       let country = [];
       countryData.data.forEach((item) => {
        country.push(`+${item}`)
       })
       commit('setCodeList',country)
       },
       //show hide footer
       showFooter({ commit },payload){commit('setShowFooter', payload)},

       //subscribe form
       submitSubscribeNews({commit},payload){
        console.log(commit)
        sendMailChimps(payload)
        notification.successPopup(i18n.t('message.subscribe'))
        // notification.sendNotificaton({msg:i18n.t('message.subscribe'),type:'success'});
       },
       //function to save submit your idea form data
       saveSubmitYourIdea({commit},payload){
       console.log(payload)
       console.log(commit)
       notification.sendNotificaton({msg:i18n.t('message.submit_idea'),type:'success'})
       },
       async saveExtraQueryOfJobList({commit},payload){
       if(payload.resume){
       //convert base64
       console.log(payload.resume)
       notification.sendNotificaton({msg:i18n.t('message.submit_query'),type:'success'})
       }
       console.log(payload)
       console.log(commit)
       },
       saveSubmitApplicationForm({commit},payload){
       if(payload.resume){
       //convert base64
        console.log(payload)
       }
       console.log(commit)
       notification.sendNotificaton({msg:'Submit Your Application successfully!',type:'success'})
       },
       sendNotificaton({commit},payload){
        Vue.$toast.open({
            message: payload.msg,
            type:payload.type,
            position:'top-right',
            duration:3000,
            pauseOnHover:true
        });
       console.log(commit)
       },
       //function to get all jobs
      getJobs({ commit },pageNumber) {
          commit('setloading', true)
          back4appService.getJobs(pageNumber)
                .then((response) => {
                    commit('setJobs', response)
                    commit('setloading', false)
                })
                .catch(error => {
                    notification.sendNotificaton({msg:error.message,type:'error'})
                })
      },
      getBlogs({ commit },pageNumber) {
            commit('setloading', true)
            back4appService.getBlogs(pageNumber)
                .then((response) => {
                    commit('setBlogs', response)
                    commit('setloading', false)
                })
                .catch(error => {
                     notification.sendNotificaton({msg:error.message,type:'error'})
                })  
      },
      setLoadingStatus({commit},status){
        commit('setloading', status)
      }


	},
	mutations:{
		setUserProfile:(state,val) => state.userProfile = val,
		setUserLoginStatus:(state,status) => state.isLoggedInUser = status,
		setUserProfileData:(state,val) => {
        console.log(val.id)
        state.isUserProfileSetup =  val.get('isProfileSetup');
			let data = {
                objectId: val.id,
                uid: val.get('uid'),
                firstName: (val.get('firstName'))?val.get('firstName'):'',
                lastName: (val.get('lastName'))?val.get('lastName'):'',
			email: (val.get('email'))?val.get('email'):'',
			phoneNumber: (val.get('phoneNumber'))?val.get('phoneNumber'):'',
			userType: val.get('userType'),
			dialCode: (val.get('dialCode'))?val.get('dialCode'):'',
			isProfileSetup: val.get('isProfileSetup'),
			status: val.get('status'),
			createdBy: val.get('createdBy'),
			day: (val.get('day'))?val.get('day'):'',
			month: (val.get('month'))?val.get('month'):'',
			year: (val.get('year'))?val.get('year'):'',
			gender: (val.get('gender'))?val.get('gender'):'',
			postalCode: (val.get('postalCode'))?val.get('postalCode'):'',
			}
			state.userProfile = data;
		},
		setHeader:(state,val) => state.isSecondHeader = val,
		setHeaderClass:(state,val) => state.isHeaderClass = val,
		setColorClass:(state,val) => state.isGreyColorClass = val,
		setCodeList:(state,val) => state.codeList = val,
		setShowFooter:(state,val) => state.isShowFooter = val,
    setloading:(state,val) => state.isLoading = val,
    setJobs:(state,jobData) => {
      if(jobData.length){
       let jobArray = []
       jobData.forEach(data => {
        let job = {
          jobId: data.id,
          title: (data.get('title'))?data.get('title'):'',
          subtitle: (data.get('subtitle'))?data.get('subtitle'):'',
          shortDescription: (data.get('shortDescription'))?data.get('shortDescription'):'',
          description: (data.get('description'))?data.get('description'):''
        }
         jobArray.push(job)
       })
       state.jobs = jobArray;
       state.totalJobs = jobData.totalJobs;
       }
    },
    setBlogs:(state,blogData) => {
      if(blogData.length){
      let blogArray = []
      blogData.forEach(data => {
        let blog = {
          blogId: data.id,
          title: (data.get('title'))?data.get('title'):'',
          publishDate: (data.get('publishDate'))?data.get('publishDate'):'',
          description: (data.get('description'))?data.get('description'):'',
          postImage: (data.get('postImage'))?data.get('postImage'):'',
        }
        blogArray.push(blog)
      })
      state.blogs = blogArray;
      state.totalBlogs = blogData.totalBlogs;
      }
    },
    setSeekerAndRecuiterInfo:(state,val) => {
      let data = {
          objectId: val.id,
          userId: val.get('userId'),
          jobSeekerId: val.get('jobSeekerId'),
          firstName: (val.get('firstName'))?val.get('firstName'):'',
          lastName: (val.get('lastName'))?val.get('lastName'):'',
          email: (val.get('email'))?val.get('email'):'',
          phoneNumber: (val.get('phoneNumber'))?val.get('phoneNumber'):'',
          dob: (val.get('dob'))?val.get('dob'):'',
          loginType: val.get('loginType'),
          gender: (val.get('gender'))?val.get('gender'):'',
          postCode: (val.get('postCode'))?val.get('postCode'):'',
          companyName: (val.get('companyName'))?val.get('companyName'):'',
          isSubscriptionDone: (val.get('isSubscriptionDone'))?val.get('isSubscriptionDone'):false
      }
      state.userProfile = data;
    },
	}
})


//send mailchimp email api calling
async  function sendMailChimps(payload){
  const data ={
    firstName:"Moiz",
    lastName:"khan",
    email:payload.email,
    tags:payload.tags
 };
// const postData = JSON.stringify(data);
const options = {
    url: 'https://api.applligent.com/',
    // url:'http://localhost:5000/',
    method: 'POST',
    headers: {
       // 'Content-Type': 'application/json charset=utf-8'
    },
    data: data
}
axios(options)
.then()
.catch(error => console.log(error))
}



