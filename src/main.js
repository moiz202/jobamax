import Vue from 'vue'
import App from './App.vue'
import router from './router';
import { store } from './store/index'
import i18n from './i18n'
import '@/assets/css/style.css'
//import bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js'
import VModal from 'vue-js-modal';
import Vuelidate from 'vuelidate';
import VueSocialSharing from 'vue-social-sharing'
import back4appService from '@/common/back4appService.js';
import VueMeta from 'vue-meta'
  
import { Bugfender } from '@bugfender/sdk';
/*To use Sentry with your Vue application*/
import * as Sentry from "@sentry/vue";
Sentry.init({
  Vue: Vue,
  dsn: "https://f920402ecf40481f9b3e2766a84f64e6@o529651.ingest.sentry.io/5648340",
});
/*End Sentory code initialication*/

/* bug fender intialization*/
Bugfender.init({
  appKey: 'i7mdQebB6hnpYZ1aBg5dWuhKjK5juKPG',
});

Vue.use(VueSocialSharing);
Vue.config.productionTip = false
Vue.use(VModal) 
Vue.use(Vuelidate)
Vue.use(VueMeta)


//get country code from ip
// fetch('https://extreme-ip-lookup.com/json/').then( res => res.json())
// .then(response => {
//     if(response.countryCode.toLowerCase() === 'fr' || response.countryCode.toLowerCase() == 'fr'){
//       localStorage.setItem('countryCode','fr')
//     } else {
//       let lang = navigator.language.split('-')[0];
//       if(lang === 'fr' || lang == 'fr'){
//          localStorage.setItem('countryCode','fr')
//       } else {
//         localStorage.setItem('countryCode','en')
//       }
//     }
// })


// use beforeEach route guard to set the language
router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0);
  document.title = 'Jobamax | ' + to.meta.title;
  // use the language from the routing param or default language
  let language = to.params.lang;
  if (!language) {
    language = 'en'
  }
  // document.documentElement.lang = language;
  //check given language exist or not
  if(!['en','fr'].includes(language)) return next('en')
  // const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
  // if (requiresAuth && !auth.currentUser) {
    // next('/')
  // } else {
 
  let headerRoutes = ['profile','media','recuiter-media','my-profile','account','personal-info','cv','chat','training','jobs','recuiter-news']
  if(headerRoutes.includes(to.name)){
    store.dispatch('changeHeader', true)
    store.dispatch('changeHeaderClass', true)
    store.dispatch('showFooter', false)
    /*if(to.name==='my-account'){
      store.dispatch('changeColor', true)
    } else {
      store.dispatch('changeColor', false)
    } */
  } else {
    let newHeader = ['create-account','job-list','contact-us','submit-application-form','success-payment','subscription','subscribe','subscribes']
    if(newHeader.includes(to.name)){
      store.dispatch('changeHeaderClass', true)
    } else {
      store.dispatch('changeHeaderClass', false)
    }
    store.dispatch('changeHeader', false)
    store.dispatch('showFooter', true)
  }  
  // set the current language for i18n.
  i18n.locale = language
  next()
// }
})

router.afterEach(() => {
  var element = document.querySelector('.navbar-collapse');
  // element.classList.remove('show');//
  // element.className = element.className.replace(/\bshow\b/g, "");
  // document.querySelector('.navbar-collapse').classList.remove('show')
  if(element){
    element.classList.contains('show') ? element.classList.remove('show'):''
  }
})


// auth.onAuthStateChanged(user => {
//   if(user){
//     store.dispatch('fetchRecuiterProfile', user)
//   }
// })

// check back4app current user login or not
var currentUser = back4appService.currentUser();
if (currentUser) {
    // localStorage.setItem('userId', currentUser.id)
    if(localStorage.getItem('userType') == 1) {
      store.dispatch('fetchSeekerProfile', currentUser.id)
    } else {
      let user = {uid:currentUser.id}
      store.dispatch('fetchRecuiterProfile', user)
    }
}

store.dispatch('fetchCountryCode')
new Vue({
    router,
    store:store,
    i18n,
    render: h => h(App)
}).$mount('#app')


