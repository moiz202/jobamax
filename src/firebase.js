import firebase from 'firebase'
import 'firebase/auth'
// import 'firebase/app'
import 'firebase/firestore'

// firebase init
const firebaseConfig = {
    apiKey: "AIzaSyDlTcBuLg7s6PqNTseYR5fdFe9KHN-ks9Y",
    authDomain: "avid-water-250605.firebaseapp.com",//
    databaseURL:'https://avid-water-250605.firebaseio.com',
    projectId: "avid-water-250605",//
    storageBucket: "avid-water-250605.appspot.com",
    messagingSenderId: "1060498395193",
    appId: "1:1060498395193:web:d6a800e25cdb7135f7fde8",
    measurementId: "G-KX1XC533PX"
};
firebase.initializeApp(firebaseConfig)

// utils
const db = firebase.firestore()
const auth = firebase.auth()


// export utils/refs
export {
  db,
  auth,
  firebase
}