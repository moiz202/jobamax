const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
	configureWebpack: {
        plugins: [new BundleAnalyzerPlugin()],
        resolve: {
        	alias:{
        		moment:'moment/src/moment'
        	}
        }
    },
	// publicPath:'/images', //for subdomian  eg.  main-domain.com/project-name
	publicPath:'/', //for root domain url eg. main-domian.com
	assetsDir:'',
	// assetsPath:'assets',
  	pluginOptions: {
	    i18n: {
	      locale: 'en',
	      fallbackLocale: 'en',
	      localeDir: 'locales',
	      enableInSFC: true
	    }
	},
	
}